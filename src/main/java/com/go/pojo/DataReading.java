package com.go.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;

/**
 * ClassName: DataReading
 * Description:
 * Date: 2021/12/14
 * @author: Cason
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataReading {

   private String carrier;
   private String deviceId;
   private String deviceType;
   private String eventId;
   private int id;
   private int isNew;
   private int lastUpdate;
   private double latitude;
   private double longitude;
   private String netType;
   private String osName;
   private String osVersion;
   private HashMap<String, Object> properties;
   private String releaseChannel;
   private String resolution;
   private String sessionId;
   private long timestamp;
   private String uid;
}
