package com.go.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author galeno
 * @Title:
 * @Description: 学生实体类
 * @date 日期2021/12/10时间20:34
 */
@Data@NoArgsConstructor@AllArgsConstructor
public class Student {
    private Integer id;
    private String name;
    private Integer age;
}
