package com.go.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ClassName: Teacher
 * Description:
 * Date: 2021/12/10
 * @author: Cason
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Teacher {
    private long id;
    private String name;
    private String className;
    private int age;
}
