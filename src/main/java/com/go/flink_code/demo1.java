package com.go.flink_code;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * ClassName: demo1
 * Description:
 * Date: 2021/12/24
 * @author: Cason
 */
public class demo1 {
   public static void main(String[] args) throws Exception {

      StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
      env.enableCheckpointing(1000);



      env.execute();

   }
}

class DoubleCheck {
   private static class SingletonHolder {
      private static final DoubleCheck INSTANCE = new DoubleCheck();
   }

   private DoubleCheck (){}

   public static final DoubleCheck getInstance() {
      return SingletonHolder.INSTANCE;
   }
}
