package com.go.flink_code;

import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ClassName: demo2
 * Description:
 * Date: 2021/12/30
 * @author: Cason
 */
public class demo2 {
   public static void main(String[] args) {
      Configuration conf = new Configuration();
      conf.setInteger("rest.port", 8081);
      StreamExecutionEnvironment env =
              StreamExecutionEnvironment.createLocalEnvironmentWithWebUI(conf);

      env.enableCheckpointing(10000);

      DataStreamSource<String> sourceStream = env.socketTextStream("cason", 8888);

      sourceStream.process(new Demo2_ProcessFunc());

      


   }
}
